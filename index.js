'use strict';

const async = require('async');
const co = require('co');
const fs = require('mz/fs');
const path = require('path');
const program = require('commander');
const request = require('request');
const yaml = require('js-yaml');

co(function * () {
  var dataDir;

  program
    .version(require('./package.json').version)
    .usage('<outputDir>')
    .arguments('<outputDir>')
    .action(outputDir => {
      dataDir = outputDir;
    })
    .parse(process.argv);

  if (typeof dataDir === 'undefined') {
    console.error('No output directory given.');
    process.exit(1);
  }

  var stats;
  try {
    stats = yield fs.stat(dataDir);
  } catch (e) {
    if (e.code === 'ENOENT') {
      console.error('Output directory does not exists.');
      process.exit(1);
    }
  }

  if (!stats.isDirectory()) {
    console.error(dataDir + ' is not a directory.');
    process.exit(1);
  }

  async.parallel((['licenses', 'categories', 'tags', 'technologies'].map(resource => {
    return cb => {
      request({
        url: 'http://www.civicstack.org/api/' + resource,
        headers: {
          Accept: 'application/json'
        }
      }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
          return cb(null, JSON.parse(body));
        }
      });
    };
  })), (err, results) => {
    if (err) {
      console.log(err);
      throw err;
    }

    var [licenses, categories, tags, technologies] = results;
    console.log('Properties ids retreived.');

    request({
      url: 'http://www.civicstack.org/api/applications/approved',
      headers: {
        Accept: 'application/json'
      }
    }, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        try {
          results = JSON.parse(body);
        } catch (e) {
          console.log(e);
          throw e;
        }

        console.log('Got', results.length, 'programs.');
        var directories = {};
        results.map(co.wrap(function * (soft) {
          soft.license = licenses.find(e => (e.id === soft.license)) || null;
          soft.category = categories.find(e => (e.id === soft.category)) || null;
          soft.tags = soft.tags.map(tagId => tags.find(e => e.id === tagId)).filter(e => e);
          soft.technology = soft.technology.map(techId => technologies.find(e => e.id === techId)).filter(e => e);
          var label = soft.name;
          var fileName = label.toLowerCase().replace(/[^a-z0-9]/g, '_') + '.yaml';

          if (!directories[fileName.slice(0, 1)]) {
            try {
              fs.statSync(path.join(dataDir, fileName.slice(0, 1)));
            } catch (e) {
              if (e.code === 'ENOENT') {
                fs.mkdirSync(path.join(dataDir, fileName.slice(0, 1)));
                console.log('Creating folder for letter ' + fileName.slice(0, 1));
              }
            }
          }
          directories[fileName.slice(0, 1)] = true;

          fs.writeFileSync(
            path.join(dataDir, fileName.slice(0, 1), fileName),
            yaml.dump(soft)
          );
        })).map(promise => promise.catch(console.log));
      }
    });
  });
});
